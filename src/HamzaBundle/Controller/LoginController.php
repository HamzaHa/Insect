<?php

namespace HamzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\UsernamePasswordToken;
use Symfony\Component\Security\Http\Event\InteractiveLoginEvent;
use Symfony\Component\HttpFoundation\Response;

use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;


class LoginController extends Controller
{


    // manual encoding of authentification
    public function authenticateAction(Request $request,$user,$pass)
    {
        // Login form to create
        // TODO -> find a way to separate the form from controllers

        $form1 = $this->get('form.factory')
            ->createNamedBuilder('login', 'form',null, array('attr' => array('id' => 'login-form')))
            ->getForm();


        //inscription form
        $form2 = $this->get('form.factory')
            ->createNamedBuilder('inscription', 'form')
            ->getForm();

        // **** debug *******
        //        var_dump($user);
        //        var_dump($pass);


        // This data is most likely to be retrieven from the Request object (from Form)
        // But to make it easy to understand ...
        //        $_username = "test";
        //        $_password = "test";

        // Retrieve the security encoder of symfony
        $factory = $this->get('security.encoder_factory');


        /// Start retrieve user
        // If using FOSUserBundle:
        $user_manager = $this->get('fos_user.user_manager');
        $user = $user_manager->findUserByUsername($user);

        // Or manually
        //        $user = $this->getDoctrine()->getManager()->getRepository("HamzaBundle:Insect")
        //            ->findOneBy(array('username' => $_username));
        /// End Retrieve user

        // Check if the user exists
        if(!$user){
            $content = $this->renderView('@HamzaBundle/Default/index.html.twig',
                                         array('error' => 'Username doesnt exists','login' => $form1->createView(),
                                               'inscription' => $form2->createView())
                                        );

            return new Response($content);
        }

        /// Start verification
        $encoder = $factory->getEncoder($user);
        $salt = $user->getSalt();

        if(!$encoder->isPasswordValid($user->getPassword(), $pass, $salt)) {
            $content = $this->renderView(
                '@HamzaBundle/Default/index.html.twig',
                array('error' => 'Username or Password not valid.','login' => $form1->createView(),
                      'inscription' => $form2->createView())
            );

            return new Response($content);

        }
        /// End Verification


        // The password matches ! proceed to set the user in session

        //Handle getting or creating the user entity likely with a posted form
        // The third parameter "main" can change according to the name of the firewall in security.yml
        $token = new UsernamePasswordToken($user, null, 'main', $user->getRoles());
        $this->get('security.token_storage')->setToken($token);

        // If the firewall name is not main, then the set value would be instead:
        // $this->get('session')->set('_security_XXXFIREWALLNAMEXXX', serialize($token));
        $this->get('session')->set('_security_main', serialize($token));

        // Fire the login event manually
        $event = new InteractiveLoginEvent($request, $token);
        $this->get("event_dispatcher")->dispatch("security.interactive_login", $event);

        /*
         * Now the user is authenticated !!!!
         */

        //redirecting user
        return $this->redirectToRoute('welcome');

        ;

    }



    public function RegisterAction(Request $request){
        // TODO : Manuel Form Validation

        $form1 = $this->get('form.factory')
            ->createNamedBuilder('login', 'form',null, array('attr' => array('id' => 'login-form')))
            ->getForm();


        $form2 = $this->get('form.factory')
            ->createNamedBuilder('inscription', 'form',null, array('attr' => array('id' => 'register-form','style' => 'display: none;')))
            //        ->add('bar', 'text')
            ->getForm();



        //        var_dump($request->request->all());
        $form = $request->request->all();
        $userManager = $this->get('fos_user.user_manager');
        $user = $userManager->createUser();
        $user->setUsername($form['username']);
        $encoder = $this->container->get('security.password_encoder');
        $encoded = $encoder->encodePassword($user, $form['password']);
        $user->setPassword($encoded);
        $user->setName($form['name']);
        $user->setAge($form['age']);
        $user->setRace($form['race']);
        $user->setEmail($form['email']);
        $user->setFamille($form['famille']);
        $user->setFood($form['food']);
        $user->setEnabled($form['food']);

        $test1=$userManager->findUserByUsername($form['username']);
        $test2=$this->getDoctrine()->getManager()->getRepository("HamzaBundle:Insect")
            ->findOneBy(array('email' => $form['email']));

        if($test1){
            $content = $this->renderView(
                '@HamzaBundle/Default/index.html.twig',
                array('error' => 'Username Already exist','login' => $form1->createView(),
                      'inscription' => $form2->createView())
            );

            return new Response($content);

        } else if ($test2){
            $content = $this->renderView(
                '@HamzaBundle/Default/index.html.twig',
                array('error' => 'Email Already exist','login' => $form1->createView(),
                      'inscription' => $form2->createView())
            );

            return new Response($content);

        }




        //        $friend->addMyFriend($user);

        $userManager->updateUser($user);
        // $userManager->flush();

        //        return new Response(
        //            'yzaaea',
        //            Response::HTTP_OK,
        //            array('Content-type' => 'application/json')
        //        );


        // Login form to create in case of error : TODO -> find a way to separate the form from controllers




        $content = $this->renderView(
            '@HamzaBundle/Default/index.html.twig',
            array('sucess' => 'User Created','login' => $form1->createView(),
                  'inscription' => $form2->createView())
        );

        return new Response($content);

    }



}
