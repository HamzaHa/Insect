<?php

namespace HamzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;


class WelcomeController extends Controller
{


    // display main
    public function welcomeAction(Request $request){

        //        $user = $this->getUser()->getId();
        //        var_dump($this->getUser()); 
        $content = $this->renderView(
            '@HamzaBundle/Default/insectPage.html.twig',
            array('user' =>  $this->getUser())
        );

        return new Response($content);

    }    

    // list my friends
    public function listAction(Request $request){

        $content = $this->renderView(
            '@HamzaBundle/Default/insectPage.html.twig',
            array('list' => $this->getUser()->getMyFriends(),
                  'user' => $this->getUser())
        );

        return new Response($content);

    }    


    // list all insects
    public function listallAction(Request $request){

        // get everyone except current user
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT u FROM HamzaBundle:Insect u WHERE u.id != :role')
            ->setParameter('role', $this->getUser()->getId() );
        $users = $query->getResult();

        //return view
        $content = $this->renderView(
            '@HamzaBundle/Default/insectPage.html.twig',
            array('all' => $users,'user' => $this->getUser())
        );

        return new Response($content);

    }    

    public function addfriendAction($id){
        // get all users
        $query = $this->getDoctrine()->getEntityManager()
            ->createQuery('SELECT u FROM HamzaBundle:Insect u WHERE u.id != :role')
            ->setParameter('role', $this->getUser()->getId() );
        $users = $query->getResult();

        
        
        //find user
        $userManager = $this->get('fos_user.user_manager');
        $friend=$userManager->findUserBy(array('id' => $id));


        $test=$this->getUser()->getMyFriends()->contains($friend);

        if($test){
            //add friend
            
            $content = $this->renderView(
                '@HamzaBundle/Default/insectPage.html.twig',
                array('all' => $users,'user' => $this->getUser(),'error' => 'Oops you are already friends'));

            return new Response($content);

        }


        // in case is not friends
        $this->getUser()->addMyfriend($friend);
        $userManager->updateUser($this->getUser());





        //return viezw
        $content = $this->renderView(
            '@HamzaBundle/Default/insectPage.html.twig',
            array('all' => $users,'user' => $this->getUser(),'sucess' => 'User added to your list',));

        return new Response($content);

    }    

    public function removefriendAction($id){
        //find user
        $userManager = $this->get('fos_user.user_manager');
        $friend=$userManager-> findUserBy(array('id' => $id));

        //delete user
        $this->getUser()->removeMyFriend($friend);
        $userManager->updateUser($this->getUser());

        // return view
        $content = $this->renderView(
            '@HamzaBundle/Default/insectPage.html.twig',
            array('list' => $this->getUser()->getMyFriends(),
                  'user' => $this->getUser())
        );


        return new Response($content);

    }


    public function profileAction(Request $request){

        //modification form
        $form2 = $this->get('form.factory')
            ->createNamedBuilder('inscription', 'form',null, array('attr' => array('id' => 'register-form','style' => 'display: none;')))
            ->getForm();

        // get current user
        $userManager = $this->get('fos_user.user_manager');
        $user=$this->getUser();


        // intercept form submission
        if('POST' === $request->getMethod()) {
            if ($request->request->has('register-submit')) {
                $info = $request->request->all();
                $user->setAge($info['age']);
                $user->setRace($info['race']);
                $user->setFamille($info['famille']);
                $user->setFood($info['food']);
                $userManager->updateUser($user);

                // display sucess
                $content = $this->renderView(
                    '@HamzaBundle/Default/editprofile.html.twig',
                    array('inscription' => $form2->createView(),
                          'user' => $this->getUser())
                );

                return new Response($content);

            }
        }

        // normal display
        $content = $this->renderView(
            '@HamzaBundle/Default/editprofile.html.twig',
            array('inscription' => $form2->createView(),
                  'user' => $this->getUser())
        );

        return new Response($content);

    }




}
