<?php



namespace HamzaBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;

use FOS\UserBundle\Model\UserManagerInterface;


class DefaultController extends Controller
{

    public function numberAction(Request $request)
    {
//        $userManager = $this->get('fos_user.user_manager');


        //        $user = $userManager->createUser();
        //        $user->setUsername('test');
        //        $encoder = $this->container->get('security.password_encoder');
        //        $encoded = $encoder->encodePassword($user, 'test');
        //        $user->setPassword($encoded);
        //        $user->setName('thisisname');
        //        $user->setAge('thisisage');
        //        $user->setRace('thisisrace');
        //        $user->setEmail('test');
        //        $user->setFamille('thisisfamille');
        //        $user->setFood('thisisfood');
        //
        //        $userManager->updateUser($user);
        //        $email_exist = $userManager->findUserByEmail('john.doe@example.com');

        // Check if the user exists to prevent Integrity constraint violation error in the insertion
        //      if($email_exist){
        //    }      


        // Login form
        $form1 = $this->get('form.factory')
            ->createNamedBuilder('login', 'form',null, array('attr' => array('id' => 'login-form')))
            ->getForm();




        //inscription form
        $form2 = $this->get('form.factory')
            ->createNamedBuilder('inscription', 'form',null, array('attr' => array('id' => 'register-form','style' => 'display: none;')))
            //        ->add('bar', 'text')
            ->getForm();



        if('POST' === $request->getMethod()) {

            //            $task = $form1->getData();
            // $form1->handleRequest($request);  
            $task = $request->request->all();

//            var_dump($request->request->all()); // debug

            if ($request->request->has('login-submit')) {

                $response = $this->forward('HamzaBundle:Login:authenticate', array(
                    'user'  => $task['username'],  
                    'pass' => $task['password'],
                ));
                return $response;
                //                $test = "form 1 value is " . $task['username'];
                //                return $this->render('@HamzaBundle/Default/index.html.twig', ['test' => $test ]);

            }

            if ($request->request->has('register-submit')) {
//                $form2->handleRequest($request);
//                $data = $form2->getData();
//                if ($form2->isSubmitted() && $form2->isValid()) {
//                    // data is an array with "name", "email", and "message" keys
//                    var_dump($data);
//                }
//                var_dump($request->request->all());
                $response = $this->forward('HamzaBundle:Login:register');
                return $response;
            }
        }

        return $this->render('@HamzaBundle/Default/index.html.twig', ['login' => $form1->createView(),
                                                                      'inscription' => $form2->createView()]);
    }

    //        $number = mt_rand(0, 100);
    //
    //        return new Response(
    //            '<html><body>Whaat number: '.$number.'</body></html>'
    //        );
    //        
    //        return $this->render('@HamzaBundle/Resources/views/Default/index.html.php');

    /*  $form = $this->createFormBuilder()
                 ->add('task', 'text')
                 ->add('dueDate', 'date')
                 ->getForm();

            $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
        // $form->getData() holds the submitted values
        // but, the original `$task` variable has also been updated
        $task = $form->getData();

        // ... perform some action, such as saving the task to the database
        // for example, if Task is a Doctrine entity, save it!
        // $em = $this->getDoctrine()->getManager();
        // $em->persist($task);
        // $em->flush();

        return $this->render('@HamzaBundle/Default/sucess.html.twig', ['test' => $task['task'] ]);
    }



    return $this->render('@HamzaBundle/Default/index.html.twig', ['form' => $form->createView(),'test' => 'btn-primary']);
    } */
}
